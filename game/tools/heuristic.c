#include "heuristic.h"

int whiteEval(ChessBoard* board)
{
    //Mastering the chessboard is important to have access to important cells.
    //The AI must maximize her mastering and minimize her opponent's.
    return board->whiteMastering - board->blackMastering;
}

int blackEval(ChessBoard* board)
{
    //Mastering the chessboard is important to have access to important cells.
    //The AI must maximize her mastering and minimize her opponent's.
    return board->blackMastering - board->whiteMastering;
}
