#ifndef CHESSBOARD_H
#define CHESSBOARD_H
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include "../constants/tables.h"
#include "../constants/constants.h"

/**
 * \file "game\chessboard.h"
 * \brief Structures and functions required to compute every movements of the pieces
 * \author Mathieu L.
 * \version 3
 */

/**
 * \enum Color
 * \brief Color of the pieces, whether white or black
 */
typedef enum
{
    WHITE=0,    /*!< White team*/
    BLACK=1     /*!< Black team*/
}Color;

/**
 * \typedef Bitboard
 * \brief A 64 bits unsigned int.
 * Allow to represent a layer of the chessboard
 */
typedef uint64_t Bitboard;

/**
 * \typedef Movement
 * \brief A 16 bits unsigned int.
 * Useful to carry informations about the movements :
 * - 6 bits to represent the start position
 * - 6 bits to represent the end position
 * - 4 bits to represent the special informations (such as promotion)
 */
typedef unsigned short Movement;

/**
 * \struct board
 * \brief Struct of the chessboard
 */
/**
 * \typedef ChessBoard
 * \brief Struct of the chessboard
 */
typedef struct board
{
    Bitboard blackRook;     /*!< Black rooks' mask*/
    Bitboard blackKnight;   /*!< Black knights' mask*/
    Bitboard blackBishop;   /*!< Black bishops' mask*/
    Bitboard blackKing;     /*!< Black king's mask*/
    Bitboard blackPawn;     /*!< Black pawns' mask*/
    Bitboard blackPieces;   /*!< Black pieces' mask*/

    Bitboard whiteRook;     /*!< White rooks' mask*/
    Bitboard whiteKnight;   /*!< White knights' mask*/
    Bitboard whiteBishop;   /*!< White bishops' mask*/
    Bitboard whiteKing;     /*!< White king's mask*/
    Bitboard whitePawn;     /*!< White pawns' mask*/
    Bitboard whitePieces;   /*!< White pieces' mask*/

    int whiteMastering;     /*!< White mastering weight over the chessboard*/
    int blackMastering;     /*!< Black mastering weight over the chessboard*/
} ChessBoard;


/**
 * \fn void initChessBoard(ChessBoard* c)
 * \brief initialize the chessboard with default values
 * \param *c
 *          chessboard pointer
 */
void initChessBoard(ChessBoard* c);

/**
 * \fn void drawBoard(ChessBoard* board)
 * \brief draw the board with ASCII characters
 * \param *board
 *          chessboard pointer
 */
void drawBoard(ChessBoard* board);

/**
 * \fn Movement* createMovements(ChessBoard* board, Color color, Color AI, int* nbMove)
 * \brief create all the possible movement for the player
 * \param *board
 *          chessboard pointer
 * \param color
 *          Color of the current player
 * \param AI
 *          Color of the AI
 * \param *nbMove
 *          Nomber of possible movements
 * \return the list of all possible movements
 */
Movement* createMovements(ChessBoard* board, Color color, Color AI, int* nbMove);

/**
 * \fn movePiece(ChessBoard* board, Movement* movement, Color c, Color AI)
 * \brief move a certain piece on the board
 * \param *board
 *          chessboard pointer
 * \param *movement
 *          movement to perform
 * \param c
 *          Color of the current player
 * \param AI
 *          Color of the AI
 * \return 1 if victory for the AI, -1 if victory for the opponent, 0 otherwise
 */
int movePiece(ChessBoard* board, Movement* movement, Color c, Color AI);

#endif // CHESSBOARD_H

