#include "turn.h"

int createPossibilies(Turn* turn)
{
    int i, nbMove;
    Movement* movements;
    movements = createMovements(&(turn->board), turn->player, turn->AIColor, &nbMove);
    Turn* t;
    int tmpIndex = 0;
    int endGame = 0;
    t = calloc(sizeof(Turn), nbMove);
    for(i = 0 ; i< nbMove ; i++)
    {
        nbrBranches++;
        t[i].son = NULL;
        t[i].time = turn->time;
        t[i].AIColor = turn->AIColor;
        t[i].player = !turn->player;
        t[i].board = turn->board;
        t[i].movement = movements[i];
        t[i].evaluation = turn->evaluation;
        t[i].alpha = turn->alpha;
        t[i].beta = turn->beta;
        t[i].nodeDepth = turn->nodeDepth-1;
        t[i].maximalDepth = turn->maximalDepth;
        endGame = movePiece(&(t[i].board), &(t[i].movement), turn->player, turn->AIColor);
        if (turn->nodeDepth == 1 || endGame)
        {
            t[i].heuristic = turn->evaluation(&(t[i].board));
        }
        else
        {
            createPossibilies(&(t[i]));
            //If there is no time, the AI stops searching
            if(*(turn->time)<=5)
                break;
        }

        if(turn->nodeDepth%2 == turn->maximalDepth%2)
        {
            //Max
            if(((t[tmpIndex].heuristic < t[i].heuristic)))
            {
                tmpIndex = i;
                //AlphaBeta algorithm
                if(turn->nodeDepth > 1)
                {
                    if(turn->beta <= t[i].heuristic)
                        break;
                    else if(turn->alpha < t[i].heuristic)
                        turn->alpha = t[i].heuristic;
                }
            }
        }
        else
        {
            //Min
            if(((t[tmpIndex].heuristic > t[i].heuristic)))
            {
                tmpIndex = i;
                //AlphaBeta algorithm
                if(turn->nodeDepth > 1)
                {
                    if(turn->alpha >= t[i].heuristic)
                        break;
                    else if(turn->beta > t[i].heuristic)
                        turn->beta = t[i].heuristic;
                }
            }
        }
    }
    //Generation of the son
    Turn* tmp;
    tmp = malloc(sizeof(Turn));
    tmp->board=t[tmpIndex].board;
    tmp->heuristic=t[tmpIndex].heuristic;
    tmp->movement=t[tmpIndex].movement;
    tmp->player=t[tmpIndex].player;
    tmp->son=t[tmpIndex].son;
    turn->son = tmp;
    turn->heuristic = tmp->heuristic;
    //If there is no movement that lead to something else than a mate, we verify there is a check situation
    if(turn->nodeDepth > 1 && (turn->nodeDepth%2 != turn->maximalDepth%2) && turn->heuristic > KING_VALUE/2)
    {
        //Drawn
        if(!check(turn))
            turn->heuristic = 0;
    }
    //Memory release
    for(i = 0 ; i< nbMove ; i++)
        free(t[i].son);
    free(movements);
    free(t);
    if(turn->nodeDepth == turn->maximalDepth)
        printf("Heuristique du coup choisi : %d\n", turn->heuristic);
    return endGame;
}
void showBestGame(Turn* turn)
{
    static int i = 0;
    printf("\n\nCoup numero %d\n", ++i);
    printf("Deplacement de %d a %d\n", (turn->movement>>START_POS)&BITS_POS, (turn->movement>>END_POS)&BITS_POS);
    printf("Action speciale du coup : %d\n", turn->movement&7);
    drawBoard(&(turn->board));
    if(turn->son != NULL)
        showBestGame(turn->son);
}

int check(Turn* turn)
{
    //Exactly the same way to check if there is check than to generate the movements
    int i, nbMove;
    Movement* movements;
    movements = createMovements(&(turn->board), !turn->player, turn->AIColor, &nbMove);
    Turn* t;
    int isCheck = 0;
    t = calloc(sizeof(Turn), nbMove);
    for(i = 0 ; i< nbMove && !isCheck; i++)
    {
        t[i].player = !turn->player;
        t[i].board = turn->board;
        t[i].movement = movements[i];
        t[i].son = NULL;
        //If a piece can kill the king, then the player is in a check situation.
        isCheck = movePiece(&(t[i].board), &(t[i].movement), t[i].player, turn->AIColor);
    }
    free(movements);
    free(t);
    return isCheck;
}
