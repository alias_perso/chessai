#include "chessboard.h"
void initChessBoard(ChessBoard* c)
{
    c->blackRook = 0;
    c->blackKnight = 0;
    c->blackBishop = 0;
    c->blackKing = 0;
    c->blackPawn = 0;
    c->whiteRook = 0;
    c->whiteKnight = 0;
    c->whiteBishop = 0;
    c->whiteKing = 0;
    c->whitePawn = 0;
    c->whiteMastering = 0;
    c->blackMastering = 0;
    c->blackPieces = 0;
    c->whitePieces = 0;
}

void drawBoard(ChessBoard* board)
{
    int i;
    char c;
    for(i = 0 ; i< SIZE*SIZE; i++)
    {
        c = '-' ;
        if(((board->blackPawn>>i)&1)==1)
            c = 'p';
        else if(((board->whitePawn>>i)&1)==1)
            c = 'P';
        else if(((board->blackKing>>i)&1)==1)
            c = 'k';
        else if(((board->whiteKing>>i)&1)==1)
            c = 'K';
        else if(((board->blackKnight>>i)&1)==1)
            c = 'n';
        else if(((board->whiteKnight>>i)&1)==1)
            c = 'N';
        else if(((board->blackRook>>i)&1)==1)
        {
            if(((board->blackBishop>>i)&1)==1)
                c = 'q';
            else
                c = 'r';
        }
        else if(((board->whiteRook>>i)&1)==1)
        {
            if(((board->whiteBishop>>i)&1)==1)
                c = 'Q';
            else
                c = 'R';
        }
        else if(((board->blackBishop>>i)&1)==1)
            c = 'b';
        else if(((board->whiteBishop>>i)&1)==1)
            c = 'B';
        else if(((board->blackPieces>>i)&1)==1)
            c = '?';
        else if(((board->whitePieces>>i)&1)==1)
            c = '!';
        printf("[%c]",c);
        if((i+1)%8 == 0)
            printf("\n");
    }
    printf("\n\n");
}

Movement* createMovements(ChessBoard* board, Color color, Color AI, int* nbMove)
{
    //The function covers all the different pieces to avoid too much function calling.
    *nbMove = 0;
    Bitboard possible, target;
    Movement* possibilities;
    possibilities = calloc(sizeof(Movement), MAX_MOVE * 16);
    Bitboard n = 0LL;
    Bitboard r = 0LL;
    Bitboard rook;
    Bitboard bishop;
    Bitboard king;
    Bitboard knight;

    if(color == BLACK)
    {
        //Bitboards initialisation
        possible = ~(board->blackPieces);
        target = ~(board->whitePieces);
        r = board->blackPawn;
        rook = board->blackRook;
        bishop = board->blackBishop;
        king = board->blackKing;
        knight = board->blackKnight;
        //Pawn movements
        while(r > 0)
        {
            if(r&1)
            {
                if((((board->blackPieces|board->whitePieces)>>(n+SIZE))&1)==0)
                {
                    possibilities[(*nbMove)++] = (n<<START_POS)+((n+SIZE)<<END_POS);
                    if(n/SIZE == BLACK_PAWN_LINE && (((board->blackPieces|board->whitePieces)>>(n+2*SIZE))&1)==0)
                        possibilities[(*nbMove)++] = (n<<START_POS)+((n+2*SIZE)<<END_POS);
                }
                if(n%SIZE!=0 && (((board->whitePieces)>>(n+(SIZE-1)))&1)==1)
                    possibilities[(*nbMove)++] = (n<<START_POS)+((n+SIZE-1)<<END_POS);
                if(n%SIZE!=7 && (((board->whitePieces)>>(n+(SIZE+1)))&1)==1)
                    possibilities[(*nbMove)++] = (n<<START_POS)+((n+SIZE+1)<<END_POS);
            }
            r >>= 1;
            n++;
        }
    }
    else
    {
        //Bitboards initialisation
        possible = ~(board->whitePieces);
        target = ~(board->blackPieces);
        r = board->whitePawn;
        rook = board->whiteRook;
        bishop = board->whiteBishop;
        king = board->whiteKing;
        knight = board->whiteKnight;
        //Pawn movements
        while(r > 0)
        {
            if(r&1)
            {
                if((((board->blackPieces|board->whitePieces)>>(n-SIZE))&1)==0)
                {
                    possibilities[(*nbMove)++] = (n<<START_POS)+((n-SIZE)<<END_POS);
                    if(n/SIZE == WHITE_PAWN_LINE && (((board->blackPieces|board->whitePieces)>>(n-2*SIZE))&1)==0)
                        possibilities[(*nbMove)++] = (n<<START_POS)+((n-2*SIZE)<<END_POS);
                }
                if(n%SIZE!=0 && (((board->blackPieces)>>(n-SIZE-1))&1)==1)
                    possibilities[(*nbMove)++] = (n<<START_POS)+((n-SIZE-1)<<END_POS);
                if(n%SIZE!=7 && (((board->blackPieces)>>(n-SIZE+1))&1)==1)
                    possibilities[(*nbMove)++] = (n<<START_POS)+((n-SIZE+1)<<END_POS);
            }
            r >>= 1;
            n++;
        }
    }

    n=0;

    //knights movements
    while(knight > 0)
    {
        if(knight&1)
        {
            if(n>=SIZE)
            {
                if(n%SIZE > 1 && ((possible>>(n-SIZE-2))&1) == 1)
                    possibilities[(*nbMove)++] = (n<<START_POS)+((n-SIZE-2)<<END_POS);
                if(n%SIZE < 6 && ((possible>>(n-SIZE+2))&1) == 1)
                    possibilities[(*nbMove)++] = (n<<START_POS)+((n-SIZE+2)<<END_POS);
                if(n>=2*SIZE)
                {
                    if(n%SIZE > 0 && ((possible>>(n-2*SIZE-1))&1) == 1)
                        possibilities[(*nbMove)++] = (n<<START_POS)+((n-2*SIZE-1)<<END_POS);
                    if(n%SIZE < 7 && ((possible>>(n-2*SIZE+1))&1) == 1)
                        possibilities[(*nbMove)++] = (n<<START_POS)+((n-2*SIZE+1)<<END_POS);
                }
            }
            if(n<SIZE*7)
            {
                if(n%SIZE > 1 && ((possible>>(n+SIZE-2))&1) == 1)
                    possibilities[(*nbMove)++] = (n<<START_POS)+((n+SIZE-2)<<END_POS);
                if(n%SIZE < 6 && ((possible>>(n+SIZE+2))&1) == 1)
                    possibilities[(*nbMove)++] = (n<<START_POS)+((n+SIZE+2)<<END_POS);
                if(n<SIZE*6)
                {
                    if(n%SIZE > 0 && ((possible>>(n+2*SIZE-1))&1) == 1)
                        possibilities[(*nbMove)++] = (n<<START_POS)+((n+2*SIZE-1)<<END_POS);
                    if(n%SIZE < 7 && ((possible>>(n+2*SIZE+1))&1) == 1)
                        possibilities[(*nbMove)++] = (n<<START_POS)+((n+2*SIZE+1)<<END_POS);
                }
            }
        }
        knight >>= 1;
        n++;
    }
    n=0;

    //Rooks movements
    while(rook > 0)
    {
        if(rook&1)
        {
            int i;
            if(n%SIZE>0)
                for(i=n-1;(i+1)%SIZE!=0 && ((possible>>i)&1)==1;i--)
                {
                    possibilities[(*nbMove)++] = (n<<START_POS)+(i<<END_POS);
                    if(((target>>i)&1)==0)break;
                }
            if(n%SIZE<7)
                for(i=n+1;i%SIZE!=0 && ((possible>>i)&1)==1;i++)
                {
                    possibilities[(*nbMove)++] = (n<<START_POS)+(i<<END_POS);
                    if(((target>>i)&1)==0)break;
                }

            if(n/SIZE>0)
                for(i=n-SIZE;i>=0 && ((possible>>i)&1)==1;i-=SIZE)
                {
                    possibilities[(*nbMove)++] = (n<<START_POS)+(i<<END_POS);
                    if(((target>>i)&1)==0)break;
                }
            if(n/SIZE<7)
                for(i=n+SIZE;i<SIZE*SIZE && ((possible>>i)&1)==1;i+=SIZE)
                {
                    possibilities[(*nbMove)++] = (n<<START_POS)+(i<<END_POS);
                    if(((target>>i)&1)==0)break;
                }
        }
        rook >>= 1;
        n++;
    }
    n=0;

    //Bishop movements
    while(bishop > 0)
    {
        if(bishop&1)
        {
            if(n>=SIZE)
            {
                int i;
                for(i=1; (n-i)%SIZE != 7 && n/SIZE >= i && ((possible>>(n-SIZE*i-i))&1)==1;i++)
                {
                    possibilities[(*nbMove)++] = (n<<START_POS)+((n-SIZE*i-i)<<END_POS);
                    if(((target>>(n-SIZE*i-i))&1)==0)break;
                }

                for(i=1;(n+i)%SIZE != 0 && n/SIZE >= i && ((possible>>(n-SIZE*i+i))&1)==1;i++)
                {
                    possibilities[(*nbMove)++] = (n<<START_POS)+((n-SIZE*i+i)<<END_POS);
                    if(((target>>(n-SIZE*i+i))&1)==0)break;
                }
            }
            if(n+SIZE<64)
            {
                int i;
                for(i=1;(n-i)%SIZE != 7 && n/SIZE+i <SIZE && ((possible>>(n+SIZE*i-i))&1)==1;i++)
                {
                    possibilities[(*nbMove)++] = (n<<START_POS)+((n+SIZE*i-i)<<END_POS);
                    if(((target>>(n+SIZE*i-i))&1)==0)break;
                }

                for(i=1;(n+i)%SIZE != 0 && n/SIZE+i <SIZE && ((possible>>(n+SIZE*i+i))&1)==1;i++)
                {
                    possibilities[(*nbMove)++] = (n<<START_POS)+((n+SIZE*i+i)<<END_POS);
                    if(((target>>(n+SIZE*i+i))&1)==0)break;
                }
            }
        }
        bishop >>= 1;
        n++;
    }
    n=0;
    //King movements
    while(king > 0)
    {
        if(king&1)
        {
            if(n>=SIZE)
            {
                if((n%SIZE!=0)&&((possible>>(n-SIZE-1))&1)==1)
                    possibilities[(*nbMove)++] = (n<<START_POS)+((n-SIZE-1)<<END_POS);
                if(((possible>>(n-SIZE))&1)==1)
                    possibilities[(*nbMove)++] = (n<<START_POS)+((n-SIZE)<<END_POS);
                if((n%SIZE!=7)&&((possible>>(n-SIZE+1))&1)==1)
                    possibilities[(*nbMove)++] = (n<<START_POS)+((n-SIZE+1)<<END_POS);
            }
            if(n<SIZE*7)
            {
                if((n%SIZE!=0)&&((possible>>(n+SIZE-1))&1)==1)
                    possibilities[(*nbMove)++] = (n<<START_POS)+((n+SIZE-1)<<END_POS);
                if(((possible>>(n+SIZE))&1)==1)
                    possibilities[(*nbMove)++] = (n<<START_POS)+((n+SIZE)<<END_POS);
                if((n%SIZE!=7)&&((possible>>(n+SIZE+1))&1)==1)
                    possibilities[(*nbMove)++] = (n<<START_POS)+((n+SIZE+1)<<END_POS);
            }
            if((n%SIZE!=0)&&((possible>>(n-1))&1)==1)
                possibilities[(*nbMove)++] = (n<<START_POS)+((n-1)<<END_POS);
            if((n%SIZE!=7)&&((possible>>(n+1))&1)==1)
                possibilities[(*nbMove)++] = (n<<START_POS)+((n+1)<<END_POS);
        }
        king >>= 1;
        n++;
    }
    return possibilities;
}


int movePiece(ChessBoard* board, Movement* movement, Color c, Color AI)
{
    //The start and end position of the boards
    unsigned char start = ((*movement)>>9)&BITS_POS;
    unsigned char end = ((*movement)>>3)&BITS_POS;
    Bitboard* b;
    b = NULL;
    int victory = 0;
    if(c == BLACK)
    {
        if((board->blackPawn>>start)&1)
        {
            //Promotion
            if(end/SIZE == 7)
            {
                (*movement) |= 4;
                board->blackPawn -= (SHIFT<<start);
                board->blackMastering -= PAWN_TABLE[63-start];
                board->blackRook += (SHIFT<<end);
                board->blackBishop+= (SHIFT<<end);
                board->blackMastering += QUEEN_TABLE[63-end];
            }
            else
            {
                b = &board->blackPawn;
                board->blackMastering += (PAWN_TABLE[63-end] - PAWN_TABLE[63-start]);
            }
        }
        else if((board->blackKnight>>start)&1)
        {
            b = &board->blackKnight;
            board->blackMastering += (KNIGHT_TABLE[63-end] - KNIGHT_TABLE[63-start]);
        }
        else if((board->blackKing>>start)&1)
        {
            b = &board->blackKing;
            board->blackMastering += (KING_TABLE[63-end] - KING_TABLE[63-start]);
        }
        else if((board->blackRook>>start)&1)
        {
            b = &board->blackRook;
            board->blackMastering += (ROOK_TABLE[63-end] - ROOK_TABLE[63-start]);
            if((board->blackBishop>>start)&1)
            {
                board->blackBishop = board->blackBishop - (SHIFT<<start) + (SHIFT<<end);
                board->blackMastering -= (ROOK_TABLE[63-end] - ROOK_TABLE[63-start]);
                board->blackMastering += (QUEEN_TABLE[63-end] - QUEEN_TABLE[63-start]);
            }
        }
        else if((board->blackBishop>>start)&1)
        {
            b = &board->blackBishop;
            board->blackMastering += (BISHOP_TABLE[63-end] - BISHOP_TABLE[63-start]);
        }
        //If there already is a piece on the end position, we destroy it
        if(((board->whitePieces>>end)&1) == 1)
        {
            if(((board->whitePawn>>end)&1) == 1)
            {
                board->whiteMastering -= PAWN_TABLE[end];
                board->whitePawn &= ~(SHIFT<<end);
            }
            else if(((board->whiteKing>>end)&1) == 1)
            {
                board->whiteKing &= ~(SHIFT<<end);
                board->whiteMastering -= KING_TABLE[end];
                //If it is the king, the game is over
                victory = (AI == BLACK ? 1 : -1);
            }
            else if(((board->whiteKnight>>end)&1) == 1)
            {
                board->whiteMastering -= KNIGHT_TABLE[end];
                board->whiteKnight &= ~(SHIFT<<end);
            }
            else if(((board->whiteRook>>end)&1) == 1)
            {
                board->whiteRook &= ~(SHIFT<<end);
                if(((board->whiteBishop >>end)&1) == 1)
                {
                    board->whiteMastering -= QUEEN_TABLE[end];
                    board->whiteBishop &= ~(SHIFT<<end);
                }
                else
                    board->whiteMastering -= ROOK_TABLE[end];
            }
            else if(((board->whiteBishop >>end)&1) == 1)
            {
                board->whiteMastering -= BISHOP_TABLE[end];
                board->whiteBishop &= ~(SHIFT<<end);
            }
        }
    }
    else
    {
        if((board->whitePawn>>start)&1)
        {
            //promotion
            if(end/SIZE == 0)
            {
                (*movement) |= 4;
                board->whitePawn -= (SHIFT<<start);
                board->whiteMastering -= PAWN_TABLE[start];
                board->whiteRook += (SHIFT<<end);
                board->whiteBishop+= (SHIFT<<end);
                board->whiteMastering += QUEEN_TABLE[end];
            }
            else
            {
                b = &board->whitePawn;
                board->whiteMastering += (PAWN_TABLE[end] - PAWN_TABLE[start]);
            }
        }
        else if((board->whiteKnight>>start)&1)
        {
            b = &board->whiteKnight;
            board->whiteMastering += (KNIGHT_TABLE[end] - KNIGHT_TABLE[start]);
        }
        else if((board->whiteKing>>start)&1)
        {
            b = &board->whiteKing;
            board->whiteMastering += (KING_TABLE[end] - KING_TABLE[start]);
        }
        else if((board->whiteRook>>start)&1)
        {
            b = &board->whiteRook;
            board->whiteMastering += (ROOK_TABLE[end] - ROOK_TABLE[start]);
            if((board->whiteBishop>>start)&1)
            {
                board->whiteBishop = board->whiteBishop - (SHIFT<<start) + (SHIFT<<end);
                board->whiteMastering -= (ROOK_TABLE[end] - ROOK_TABLE[start]);
                board->whiteMastering += (QUEEN_TABLE[end] - QUEEN_TABLE[start]);
            }
        }
        else if((board->whiteBishop>>start)&1)
        {
            b = &board->whiteBishop;
            board->whiteMastering += (BISHOP_TABLE[end] - BISHOP_TABLE[start]);
        }
        //If there already is a piece on the end position, we destroy it
        if(((board->blackPieces>>end)&1) == 1)
        {
            if(((board->blackPawn>>end)&1) == 1)
            {
                board->blackMastering -= PAWN_TABLE[63-end];
                board->blackPawn &= ~(SHIFT<<end);
            }
            else if(((board->blackKing>>end)&1) == 1)
            {
                board->blackKing &= ~(SHIFT<<end);
                board->blackMastering -= KING_TABLE[63-end];
                //If it is the king, the game is over
                victory = (AI == WHITE ? 1 : -1);
            }
            else if(((board->blackKnight>>end)&1) == 1)
            {
                board->blackMastering -= KNIGHT_TABLE[63-end];
                board->blackKnight &= ~(SHIFT<<end);
            }
            else if(((board->blackRook>>end)&1) == 1)
            {
                board->blackRook &= ~(SHIFT<<end);
                if(((board->blackBishop >>end)&1) == 1)
                {
                    board->blackMastering -= QUEEN_TABLE[63-end];
                    board->blackBishop &= ~(SHIFT<<end);
                }
                else
                    board->blackMastering -= ROOK_TABLE[63-end];
            }
            else if(((board->blackBishop >>end)&1) == 1)
            {
                board->blackMastering -= BISHOP_TABLE[63-end];
                board->blackBishop &= ~(SHIFT<<end);
            }
        }
    }
    if(b != NULL)
        *b = *b - (SHIFT<<start) + (SHIFT<<end);
    //Update the new positions of all the pieces of the same color.
    board->blackPieces = board->blackRook|board->blackKnight|board->blackBishop|board->blackKing|board->blackPawn;
    board->whitePieces = board->whiteRook|board->whiteKnight|board->whiteBishop|board->whiteKing|board->whitePawn;
    return victory;
}
