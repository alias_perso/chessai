#ifndef SERVER_H
#define SERVER_H
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <winsock2.h>
#include <windows.h>
#include "../game/chessboard.h"
#include "server.h"
#include "piece.h"
#include "../game/game.h"
#include "../cJSON/cJSON.h"
//#pragma comment(lib,"ws2_32.lib") //Winsock Library
/**
 * \file "server\server.h"
 * \brief Function to communicate with the API
 * \author Mathieu L.
 * \version 1
 */

/**
 * \def API_HOST
 * \brief Server address
 */
#define API_HOST "chess.ruby.labs.adrien-thebault.fr"

/**
 * \def API_PORT
 * \brief Port on the server
 */
#define API_PORT 80
/**
 * \fn char* HTTPReq(int id)
 * \brief Returns the data given by the server about the state of the game
 * \param id
 *          game id
 * \return the informations about the current game
 */
char* HTTPReq(int id);

/**
 * \fn Color parseChessBoard(Game* game)
 * \brief Parse the datas and edit the chessboard
 * \param *game
 *          the game to be updated
 * \return The color of the current player
 */
Color parseChessBoard(Game* game);

/**
 * \fn void sendMovement(Game* game)
 * \brief Send the movement to update the server
 * \param *game
 *          the game to be updated
 */
void sendMovement(Game* game);
#endif // SERVER_H
