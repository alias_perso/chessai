#include "server.h"

char* HTTPReq(int id)
{
    //Windows HTTP request
    WSADATA wsaData;
    if (WSAStartup(MAKEWORD(2,2), &wsaData) != 0)
    {
        printf("WSAStartup failed.\n");
        exit(1);
    }
    SOCKET Socket = socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
    struct hostent *host;
    host = gethostbyname(API_HOST);
    SOCKADDR_IN SockAddr;
    SockAddr.sin_port=htons(API_PORT);
    SockAddr.sin_family=AF_INET;
    SockAddr.sin_addr.s_addr = *((unsigned long*)host->h_addr);
    if (connect(Socket,(SOCKADDR*)(&SockAddr),sizeof(SockAddr)) != 0)
    {
        printf("Could not connect\n");
        exit(1);
    }
    char req[5000];
    sprintf(req, "GET /api/get/%d HTTP/1.1\r\nHost: %s\r\nUser-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:43.0) Gecko/20100101 Firefox/43.0 Iceweasel/43.0.4\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\nAccept-Language: en-US,en;q=0.5\r\nCache-Control: max-age=0\r\nConnection: keep-alive\r\n\r\n", id, API_HOST);
    //Send the packet
    send(Socket, req, strlen(req), 0);
    char buffer[1024*1000];
    int len;
    len = recv(Socket,buffer,1024*10,0);
    int i,z;
    //Remove the useless part : keep only the JSon part
    for(i=0;!(buffer[i]=='\r' && buffer[i+1]=='\n' && buffer[i+2]=='\r' && buffer[i+3]=='\n');i++);
    i+=4;
    char* buffer2;
    buffer2 = calloc(sizeof(char),len);
    for(z=0;i<len;i++)
        buffer2[z++] = buffer[i];
    closesocket(Socket);
    WSACleanup();
    return buffer2;
}

Color parseChessBoard(Game* game)
{
    //Functions pointers tab to update the chessboard
    static void (*createPiece[])(ChessBoard*, int) =
    {
        createWhiteBishop, createWhiteKing, createWhiteKnight, createWhitePawn, createWhiteQueen, createWhiteRook, NULL,NULL,
        createBlackBishop, createBlackKing, createBlackKnight, createBlackPawn, createBlackQueen, createBlackRook, NULL, NULL
    };

    char* response;
    cJSON* json;
    Color player;
    cJSON* chessboard;
    cJSON* line;
    response = HTTPReq(game->id);
    //JSon parsing
    json = cJSON_Parse(response);
    player = (cJSON_GetObjectItem(json, "round")->child->valueint)>>3;
    chessboard = cJSON_GetObjectItem(json, "chessboard");
    json = cJSON_GetObjectItem(json, "round");
    game->time = 30;//cJSON_GetObjectItem(json, "duration")->valueint;

    signed char i, j;
    unsigned char piece;
    game->board.blackMastering = 0;
    game->board.whiteMastering = 0;
    for(i = 0; i<SIZE; i++)
    {
        line = cJSON_GetArrayItem(chessboard, i);
        for(j = 0; j<SIZE; j++)
        {
            if(cJSON_GetArrayItem(line, j)->type != cJSON_NULL)
            {
                piece = cJSON_GetArrayItem(line, j)->valueint;
                //Update the board
                createPiece[piece](&game->board, 63-(i*SIZE+j));
            }
        }
    }
    game->board.blackPieces = game->board.blackRook|game->board.blackKnight|game->board.blackBishop|game->board.blackKing|game->board.blackPawn;
    game->board.whitePieces = game->board.whiteRook|game->board.whiteKnight|game->board.whiteBishop|game->board.whiteKing|game->board.whitePawn;
    free(response);
    return player;
}

void sendMovement(Game* game)
{
    //Create the HTTP packet
    Movement m = game->movement;
    int gameID = game->id;
    WSADATA wsaData;
    if (WSAStartup(MAKEWORD(2,2), &wsaData) != 0)
    {
        printf("WSAStartup failed.\n");
        exit(1);
    }
    SOCKET Socket = socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
    struct hostent *host;
    host = gethostbyname(API_HOST);
    SOCKADDR_IN SockAddr;
    SockAddr.sin_port=htons(API_PORT);
    SockAddr.sin_family=AF_INET;
    SockAddr.sin_addr.s_addr = *((unsigned long*)host->h_addr);
    if (connect(Socket,(SOCKADDR*)(&SockAddr),sizeof(SockAddr)) != 0)
    {
        printf("Could not connect");
        exit(1);
    }
    char from[3] = "";
    char to[10] = "";
    //Write the datas into the adapted format
    sprintf(from, "%c%d\0", 97+((63-((m>>START_POS) & BITS_POS))%SIZE),1+((63-((m>>START_POS) & BITS_POS))/SIZE));
    sprintf(to, "%c%d\0", 97+((63-((m>>END_POS) & BITS_POS))%SIZE),1+((63-((m>>END_POS) & BITS_POS))/SIZE));

    if((m&7)!=0)
    {
        //Promotion
        if((m&7)==4)
        {
            sprintf(to,"%c%d=queen\0",97+((63-((m>>END_POS) & BITS_POS))%SIZE),1+((63-((m>>END_POS) & BITS_POS))/SIZE));
        }
        //TODO :
        //Petit roque
        //Grand roque
    }
    char req[5000];
    printf("Coup le plus interessant : %s -> %s\n",from, to);
    sprintf(req, "GET /api/move/%d/%s/%s HTTP/1.1\r\nHost: %s\r\nUser-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:43.0) Gecko/20100101 Firefox/43.0 Iceweasel/43.0.4\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\nAccept-Language: en-US,en;q=0.5\r\nCache-Control: max-age=0\r\nConnection: keep-alive\r\n\r\n", gameID, from, to, API_HOST);
    //Send the movement to the server
    send(Socket, req, strlen(req), 0);
}
