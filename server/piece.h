#ifndef PIECE_H
#define PIECE_H
#include <stdio.h>
#include <stdlib.h>
#include "../game/chessboard.h"

/**
 * \file "server\piece.h"
 * \brief Creation of all pieces on a chessboard from a JSon file
 * \author Mathieu L.
 * \version 1
 */

/**
 * \fn void createBlackPawn(ChessBoard* board, int pos)
 * \brief Create a black pawn at a given position on the chessboard
 * \param *board
 *          Chessboard pointer
 * \param pos
 *          Piece's position
 */
void createBlackPawn(ChessBoard* board, int pos);

/**
 * \fn void createBlackRook(ChessBoard* board, int pos)
 * \brief Create a black rook at a given position on the chessboard
 * \param *board
 *          Chessboard pointer
 * \param pos
 *          Piece's position
 */
void createBlackRook(ChessBoard* board, int pos);

/**
 * \fn void createBlackQueen(ChessBoard* board, int pos)
 * \brief Create a black queen at a given position on the chessboard
 * \param *board
 *          Chessboard pointer
 * \param pos
 *          Piece's position
 */
void createBlackQueen(ChessBoard* board, int pos);

/**
 * \fn void createBlackKing(ChessBoard* board, int pos)
 * \brief Create a black king at a given position on the chessboard
 * \param *board
 *          Chessboard pointer
 * \param pos
 *          Piece's position
 */
void createBlackKing(ChessBoard* board, int pos);

/**
 * \fn void createBlackBishop(ChessBoard* board, int pos)
 * \brief Create a black bishop at a given position on the chessboard
 * \param *board
 *          Chessboard pointer
 * \param pos
 *          Piece's position
 */
void createBlackBishop(ChessBoard* board, int pos);

/**
 * \fn void createBlackKnight(ChessBoard* board, int pos)
 * \brief Create a black knight at a given position on the chessboard
 * \param *board
 *          Chessboard pointer
 * \param pos
 *          Piece's position
 */
void createBlackKnight(ChessBoard* board, int pos);

/**
 * \fn void createWhitePawn(ChessBoard* board, int pos)
 * \brief Create a white pawn at a given position on the chessboard
 * \param *board
 *          Chessboard pointer
 * \param pos
 *          Piece's position
 */
void createWhitePawn(ChessBoard* board, int pos);

/**
 * \fn void createWhiteRook(ChessBoard* board, int pos)
 * \brief Create a white rook at a given position on the chessboard
 * \param *board
 *          Chessboard pointer
 * \param pos
 *          Piece's position
 */
void createWhiteRook(ChessBoard* board, int pos);

/**
 * \fn void createWhiteQueen(ChessBoard* board, int pos)
 * \brief Create a white queen at a given position on the chessboard
 * \param *board
 *          Chessboard pointer
 * \param pos
 *          Piece's position
 */
void createWhiteQueen(ChessBoard* board, int pos);

/**
 * \fn void createWhiteKing(ChessBoard* board, int pos)
 * \brief Create a white king at a given position on the chessboard
 * \param *board
 *          Chessboard pointer
 * \param pos
 *          Piece's position
 */
void createWhiteKing(ChessBoard* board, int pos);

/**
 * \fn void createWhiteBishop(ChessBoard* board, int pos)
 * \brief Create a white bishop at a given position on the chessboard
 * \param *board
 *          Chessboard pointer
 * \param pos
 *          Piece's position
 */
void createWhiteBishop(ChessBoard* board, int pos);

/**
 * \fn void createWhiteKnight(ChessBoard* board, int pos)
 * \brief Create a white knight at a given position on the chessboard
 * \param *board
 *          Chessboard pointer
 * \param pos
 *          Piece's position
 */
void createWhiteKnight(ChessBoard* board, int pos);
#endif // PIECE_H
