var searchData=
[
  ['beta',['beta',['../structturn.html#a684b1df447558ad94b70028b3292227e',1,'turn']]],
  ['bishop_5fvalue',['BISHOP_VALUE',['../constants_8h.html#a338953e0259ef326ec31dba538fcd420',1,'constants.h']]],
  ['bitboard',['Bitboard',['../chessboard_8h.html#a931da3934dd0d7dbe9e1b06a02adabe4',1,'chessboard.h']]],
  ['bits_5fpos',['BITS_POS',['../constants_8h.html#af98dfe80c4f119143a7b4e1203a3817a',1,'constants.h']]],
  ['black',['BLACK',['../chessboard_8h.html#ab87bacfdad76e61b9412d7124be44c1caf77fb67151d0c18d397069ad8c271ba3',1,'chessboard.h']]],
  ['black_5fpawn_5fline',['BLACK_PAWN_LINE',['../constants_8h.html#ae976a5024748ed4c0b766d506bb50974',1,'constants.h']]],
  ['blackbishop',['blackBishop',['../structboard.html#ae52e5a4018379528a89f69968bb7378c',1,'board']]],
  ['blackeval',['blackEval',['../heuristic_8h.html#ae46bf92ff2396c2b4282e0d5f74cc806',1,'heuristic.c']]],
  ['blackking',['blackKing',['../structboard.html#a7da85e8008c37529352340b139c79cb0',1,'board']]],
  ['blackknight',['blackKnight',['../structboard.html#ad65b56ff110ba9ed17f5f42f6e5d13cd',1,'board']]],
  ['blackmastering',['blackMastering',['../structboard.html#a79db081f959f05f4729222e3592ba5b2',1,'board']]],
  ['blackpawn',['blackPawn',['../structboard.html#a1fde5d4e0f17d925325c9a11eb07e898',1,'board']]],
  ['blackpieces',['blackPieces',['../structboard.html#a4eb8b5752a73ccd95a9bda4a4153e03c',1,'board']]],
  ['blackrook',['blackRook',['../structboard.html#abf2d67c7407bf5f48bf54dc02a51fa77',1,'board']]],
  ['board',['board',['../structboard.html',1,'board'],['../structgame.html#a1d143fbb94c384ba999c4b78bd08445d',1,'game::board()'],['../structturn.html#a1d143fbb94c384ba999c4b78bd08445d',1,'turn::board()']]]
];
