var searchData=
[
  ['white',['WHITE',['../chessboard_8h.html#ab87bacfdad76e61b9412d7124be44c1ca283fc479650da98250635b9c3c0e7e50',1,'chessboard.h']]],
  ['white_5fpawn_5fline',['WHITE_PAWN_LINE',['../constants_8h.html#ae574b70a80914b2a9eea7eb26e9d772e',1,'constants.h']]],
  ['whitebishop',['whiteBishop',['../structboard.html#a68a5b0c254ad58163581307a5417b7fa',1,'board']]],
  ['whiteeval',['whiteEval',['../heuristic_8h.html#a893186d83929f267dab050cd8ad0ab04',1,'heuristic.c']]],
  ['whiteking',['whiteKing',['../structboard.html#ac722058f226a4a9a5d60d0add1e34c7c',1,'board']]],
  ['whiteknight',['whiteKnight',['../structboard.html#a6e4b0b19cda6b8e17e06af41c6ca52c0',1,'board']]],
  ['whitemastering',['whiteMastering',['../structboard.html#abd2803e3464270cb2690d48aa1844bc5',1,'board']]],
  ['whitepawn',['whitePawn',['../structboard.html#af89e867046fef2394afa897579c86423',1,'board']]],
  ['whitepieces',['whitePieces',['../structboard.html#a66f26141fe3cbfa872c9a187ed051936',1,'board']]],
  ['whiterook',['whiteRook',['../structboard.html#aacb353cd9c40bde6574b1365940958af',1,'board']]]
];
